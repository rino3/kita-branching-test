from elasticsearch import Elasticsearch, helpers
import configparser

config = configparser.ConfigParser()
config.read('conf.ini')

es = Elasticsearch(
    cloud_id=config['ELASTIC']['cloud_id'],
    basic_auth=(config['ELASTIC']['user'], config['ELASTIC']['password'])
)
# es.index(
#  index='kita-index',
#  document={
#   'character': 'Aragon',
#   'quote': 'It is not this day.'
#  })

print(es.search(index='kita-index'))